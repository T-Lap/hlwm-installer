# cd + lsd
cdl() {
	cd "$@" &&  lsd
}

# Show the wether at your place with no argument or take city as argument.
wether() {
	curl wttr.in/$1
}

# ????????????
preexec() {
  timer=$(($(date +%s%0N)/1000000))
}

# Call FFF-Fucking fast Filemanager and exit at current directory. 
f() {
    fff "$@"
    cd "$(cat "${XDG_CACHE_HOME:=${HOME}/.cache}/fff/.fff_d")"
}

# Show markdown file in Browser.
showmd() {
	to_show=$(basename -s ".md" "${1}")
	pandoc -s -c "$HOME/.local/share/pandoc/css/github-pandoc.css" "${1}" > /tmp/${to_show}.html
	xdg-open "/tmp/${to_show}.html"
}

# Countdown whith alarm. 
countdown() {
    start="$(( $(date '+%s') + $1))"
    while [ $start -ge $(date +%s) ]; do
        time="$(( $start - $(date +%s) ))"
        printf '%s\r' "$(date -u -d "@$time" +%H:%M:%S)"
        sleep 0.1
    done
    line; line_t " Your $1 sec. are over "; line
    # while true; do spd-say -w "Your $1 seconds are over" ; done
    mpv --loop /usr/share/sounds/Pop/stereo/alert/alarm-clock-elapsed.oga
}

# Show all quick notes from t-note by date.
shownote() {
	for i in ~/.notes/By_Date/*; do line 3 ; echo $i ;line 4 - ; ccat $i ; done
}

# Terminal header, need line script.
lzsh() {
#	line | lolcat
line_to "${USER}@${HOST}  ZSH   $(date '+%a %d %b %Y   %H:%M')" 5
# line | lolcat
}
lbash() {
#	line | lolcat
line_to "${USER}@${HOST}  BASH   $(date '+%a %d %b %Y   %H:%M')" 6
# line | lolcat
}

# Only zsh, print text to position in a line with color option.
tprint() {
	pos=$1
	text=$2
	color=$3
	[[ -n $color ]] && tput setaf $color
  printf "%*s\n" $(((${#text}+$(tput cols))*${pos})) "$text"
	tput sgr 0  
}
# Helptext for tprint.
tprint-help() {
  line
  echo "Use only ZSH: tprint <position 0.1-0.9> \"Your text\" <color number 1-7>
With BASH please use: center-print \"Your text\" "
  line
}

center-print() {
  alltext="$@"
  printf "%*s\n" $(((${#alltext}+$(tput cols))/2)) "$alltext"
}

