#!/usr/bin/bash
#This script will install and configur Herbstluftwm.
#by Thomas Pautler 11/2022
#
cd ~/
echo "---- Speed up DNF ----"
sudo echo -e "max-parallel-downloads=10\nfastestmirrow=True" >> /etc/dnf/dnf.conf
##Install from Fedora repos##
echo "---- INSTALL FROM FEDORA REPOS ----"
sudo dnf install -y herbstluftwm polybar picom micro alacritty ranger lxappearance chromium xrandr feh adwaita-gtk2-theme arc-theme rofi thunar xclip
##Install from Git##
#FFF file manager
echo "---- INSTALL FFF FILE MANAGER FROM GIT ----"
git clone https://github.com/dylanaraps/fff.git
cd fff
sudo make install
echo "---- SETUP FFF IN .BASHRC ----"
echo -e "# Run 'fff' with 'f' or whatever you decide to name the function.
f() {
    fff \"$@\"
    cd \"\$(cat \"\${XDG_CACHE_HOME:=\${HOME}/.cache}/fff/.fff_d\")\"
}" >> ~/.bashrc
cd ~
#HLWM Autostart file
echo "---- COPY CONFIG FILES ----"
mkdir -p ~/.config/herbstluftwm/
cp ~/hlwm-installer/config/herbstluftwm/* ~/.config/herbstluftwm/
sed -i 's/f37-01-night.png/f37-02-night.png/' ~/.config/herbstluftwm/autostart
#micro config
mkdir -p ~/.config/micro/
cp -r ~/hlwm-installer/config/micro/* ~/.config/micro/
#polybar config
mkdir -p ~/.config/polybar/
cp ~/hlwm-installer/config/polybar/* ~/.config/polybar/
#rofi config
mkdir -p ~/.config/rofi/
echo "@theme \"/usr/share/rofi/themes/Monokai.rasi\"" > ~/.config/rofi/config.rasi
#HLWM color
echo "---- INSTALL CHANGE-COLOR APP ----"
sudo cp ~/hlwm-installer/src/accent-color /usr/bin/
#set theme
mkdir -p ~/.config/gtk-3.0/
cp ~/hlwm-installer/config/gtk-3.0/* ~/.config/gtk-3.0/
#Create alacritty config file
echo "---- CREATE ALACRITTY CONFIG FILE ----"
mkdir -p ~/.config/alacritty/
echo -e "window:
    padding:
        x: 15
        y: 15
    opacity: 0.9
font:
    size: 12" > ~/.config/alacritty/alacritty.yml
##Finishing
echo "---- Install finished ----
-- Please reboot --"
