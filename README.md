# fedora-hlwm-installer


### How to use Everything?
On the official download page of Fedora you just need to scroll down and download the "Fedora Everything ISO". Follow the Anaconda installer and after a reboot you will find yourself at a tty login like with Arch. From there on it's up to you.

### How do I go from there.
Over the last Years I have become a big fan of tiling window manager and my fevered one is HebstluftWM. So I will build my environment around it.  
For the start we need to install some applications and services to be able to change in to a graphical desktop.  
I uploaded a repository with the most important configuration files. Feel free to use them for the next steps.

```bash
sudo dnf install -y git
git clone https://gitlab.com/T-Lap/hlwm-installer.git
```
When you `cd hlwm-installer` you will find a you will find a **install-hlwm.sh** and a **workstation-install-hlwm.sh** script. At this point you can make your live easier and just run `./install-hlwm.sh` if you on an Fedora Everything install or `./workstation-install-hlwm.sh` if you are on a Fedora Workstation install and all will be installed and configured for you to just `reboot` on Fedora Everything or logout and login to Herbstluftwm on the Workstation.  
If you like to setup the desktop by your self just open the install script in a text editor tweak it or follow the steps manually with your spin.  
Most of the steps and tools can also be used in other systems like Arch or Ubuntu. (So fare I know only the xorg-x11 packs specific to Fedora)

### What is done for you
The installer just setup a very basic desktop with some tools I created and some tools I like.

#### Accent-Color
This is just a little script I made to change the window decoration color and the primary color of Polybar and is the reason for installing Zenity.
To use it just type `accent-color` in the terminal.

#### FFF File manager
This tool with the setup in the .bashrc or .zshrc file let me by typing `f` in the terminal to navigate to the file system and by using `q` I'm left where I navigate to.

#### Auto startx under Fedora Everything
The install script will setup a .xinit file and add a statement to the .bash_profile file. So when you login at the tty the xserver will start automatic and get you to your desktop.

### HebstluftWM short cuts
This are just my preferred short cuts you will be able to change them in your `.config/herbstluftwm/autostart` file.  
As a modifier key I use the Win key.  
- Mod-Return		open Terminal  
- Mod-q				close the focused window  
- Mod-Shift-q		logout  
- Mod-d 			Rofi menu  
For more please check the autostart file.

> I hope you have fun.
