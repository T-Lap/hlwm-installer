#!/usr/bin/bash
#This script will install and configur Herbstluftwm.
#by Thomas Pautler 11/2022
#
cd ~/
##Install from Fedora repos##
echo "---- INSTALL FROM FEDORA REPOS ----"
sudo dnf install -y xorg-x11* herbstluftwm polybar picom micro alacritty ranger lxappearance chromium xrandr zenity f37-backgrounds-gnome nitrogen adwaita-gtk2-theme rofi thunar
##Install from Git##
#FFF file manager
echo "---- INSTALL FFF FILE MANAGER FROM GIT ----"
git clone https://github.com/dylanaraps/fff.git
cd fff
sudo make install
echo "---- SETUP FFF IN .BASHRC ----"
echo -e "# Run 'fff' with 'f' or whatever you decide to name the function.
f() {
    fff \"$@\"
    cd \"\$(cat \"\${XDG_CACHE_HOME:=\${HOME}/.cache}/fff/.fff_d\")\"
}" >> ~/.bashrc
cd ~
#HLWM Autostart file
echo "---- COPY HLWM AUTOSTART FILE ----"
mkdir -p ~/.config/herbstluftwm/
cp /etc/xdg/herbstluftwm/autostart ~/.config/herbstluftwm/
#HLWM color
echo "---- INSTALL CHANGE-COLOR APP ----"
git clone https://gitlab.com/T-Lap/hlwm-tools.git
cd hlwm-tools/hlwincolor/
sudo ./install.sh
cd ~
#Create alacritty config file
echo "---- CREATE ALACRITTY CONFIG FILE ----"
mkdir -p ~/.config/alacritty/
echo -e "window:
    padding:
        x: 15
        y: 15
    opacity: 0.9
font:
    size: 9" > ~/.config/alacritty/alacritty.yml
#Creating .xinitrc
echo "exec herbstluftwm" >> ~/.xinitrc
##Finishing
echo "---- Install finished ----
-- You can now use: startx --"
